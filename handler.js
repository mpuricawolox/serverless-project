'use strict';
const SAMPLE_ENV_VAR = process.env.SAMPLE_ENV_VAR;
module.exports.log = (event, context_, callback) => {
  const { sampleInput } = event;

  console.log({ sampleInput, SAMPLE_ENV_VAR }, new Date);

  callback(null, 'OK');
};
